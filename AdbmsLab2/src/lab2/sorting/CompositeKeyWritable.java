package lab2.sorting;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;

public class CompositeKeyWritable implements WritableComparable<CompositeKeyWritable> {

	String depNo, lastname;

	public CompositeKeyWritable() {
		// required by the reducer
	}

	public CompositeKeyWritable(String depNo, String lastName) {
		this.depNo = depNo;
		this.lastname = lastName;
	}

	@Override
	public int compareTo(CompositeKeyWritable o) {
		// TODO Auto-generated method stub
		int result = depNo.compareTo(o.depNo);

		if (result == 0) {
			result = lastname.compareTo(o.lastname);
		}
		return result;
	}

	public String getDepNo() {
		return depNo;
	}

	public void setDepNo(String depNo) {
		this.depNo = depNo;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
		// TODO Auto-generated method stub

		depNo = WritableUtils.readString(arg0);
		lastname = WritableUtils.readString(arg0);

	}

	@Override
	public void write(DataOutput d) throws IOException {
		// TODO Auto-generated method stub
		WritableUtils.writeString(d, depNo);
		WritableUtils.writeString(d, lastname);

	}

	@Override
	public String toString() {
		return new StringBuilder().append(depNo).append("\t").append(lastname).toString();
	}
}
