package com.me.mr;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class Lab2Reducer extends Reducer<CompositeKeyWritable, NullWritable, CompositeKeyWritable, NullWritable> {

	@Override
	protected void reduce(CompositeKeyWritable keyIn, Iterable<NullWritable> valueIn, Context context)
			throws IOException, InterruptedException {
		System.out.println("Inside reducer-------====------++++++++");
		for (NullWritable nw : valueIn) {
			//System.out.println("thisis for value  " + keyIn);
			context.write(keyIn, NullWritable.get());
		}

	}

}
