package com.me.mr;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Sorting {

	public static void main(String args[]) throws InterruptedException {
		try {
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, "Secondary sorting");
			job.setJarByClass(Sorting.class);
			job.setMapperClass(Lab2Mapper.class);

			job.setReducerClass(Lab2Reducer.class);
			job.setMapOutputKeyClass(CompositeKeyWritable.class);
			job.setMapOutputValueClass(NullWritable.class);
			job.setGroupingComparatorClass(GroupingComparator.class);

			job.setOutputKeyClass(CompositeKeyWritable.class);
			job.setOutputValueClass(NullWritable.class);
			// job.setInputFormatClass(TextInputFormat.class);

			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileOutputFormat.setOutputPath(job, new Path(args[1]));

			// Clean up the output directory
			Path outPath = new Path(args[1]);
			outPath.getFileSystem(conf).delete(outPath, true);

			System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (IOException | ClassNotFoundException e) {
			Logger.getLogger(Sorting.class.getName()).log(Level.SEVERE, "MR fucked", e);
		}
	}

}
