package com.me.mr;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Lab2Mapper extends Mapper<Object, Text, CompositeKeyWritable, NullWritable> {
	@Override
	public void map(Object key, Text value, Context context) {
		//System.out.println("inside mapper============================================================================");

		String values[] = value.toString().split("\t");
		CompositeKeyWritable cw = new CompositeKeyWritable(values[6], values[3]);

		try {
			context.write(cw, NullWritable.get());

		} catch (IOException | InterruptedException ex) {
			System.out.println("Error msg  ex" + ex.getMessage());
		}
	}
}
