package com.me.mr;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class GroupingComparator extends WritableComparator {

	protected GroupingComparator() {
		super(CompositeKeyWritable.class, true);
	}

	@Override
	public int compare(WritableComparable w1, WritableComparable w2) {
		CompositeKeyWritable cw1 = (CompositeKeyWritable) w1;
		CompositeKeyWritable cw2 = (CompositeKeyWritable) w2;
		return cw1.getDepNo().compareTo(cw2.getDepNo());
	}

}
