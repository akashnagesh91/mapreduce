package me.question.two;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class Question2Reducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {

	@Override
	public void reduce(Text keyIn, Iterable<DoubleWritable> valueIn, Context context)
			throws IOException, InterruptedException {
		int i = 0;
		Double sum = 0.00;
		for (DoubleWritable d : valueIn) {
			i++;
			sum += d.get();
		}
		context.write(keyIn, new DoubleWritable(sum / i));
	}

}
