package me.question.three;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class GenderMapper extends Mapper<Object, Text, Text, IntWritable> {

	final IntWritable valueOut = new IntWritable(1);
	final Text keyOut = new Text();

	@Override
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
		String row = value.toString();
		String[] columns = row.split("::");
		if (columns.length < 5) {
			return;
		}
		keyOut.set(columns[1]);
		context.write(keyOut, valueOut);
	}

}
