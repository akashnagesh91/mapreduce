package lab6;

public class Person {
	int id;
	String firstName;
	String lastName;
	int birthYear;

	public Person(int id, String firstName, String lastName, int birthYear) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthYear = birthYear;
	}
}
