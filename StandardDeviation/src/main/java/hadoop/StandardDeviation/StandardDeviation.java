package hadoop.StandardDeviation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class StandardDeviation {

	public static class Map extends Mapper<Object, Text, Text, DoubleWritable> {

		@Override
		protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			try {
				String row[] = value.toString().split(",");
				String stock = row[1];
				String price = row[4];
				double pri = Double.parseDouble(price);
				context.write(new Text(stock), new DoubleWritable(pri));
			} catch (Exception e) {
				System.out.println(e);
			}
		}

	}

	public static class Reducer
			extends org.apache.hadoop.mapreduce.Reducer<Text, DoubleWritable, Text, MedianSDCustomWritable> {

		private MedianSDCustomWritable result = new MedianSDCustomWritable();
		private List<Double> list = new ArrayList<>();
		private double sum = 0;
		private int count = 0;

		@Override
		protected void reduce(Text keyIn, Iterable<DoubleWritable> valuesIn, Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			for (DoubleWritable dw : valuesIn) {
				sum += dw.get();
				count++;
			}
		}
	}
}
