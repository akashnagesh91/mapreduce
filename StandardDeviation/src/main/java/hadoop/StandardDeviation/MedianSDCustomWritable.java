package hadoop.StandardDeviation;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class MedianSDCustomWritable implements Writable {

	private double median, standardDeviation;

	public double getMedian() {
		return median;
	}

	public void setMedian(double median) {
		this.median = median;
	}

	public double getStandardDeviation() {
		return standardDeviation;
	}

	public void setStandardDeviation(double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
		median = arg0.readDouble();
		standardDeviation = arg0.readDouble();
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		// TODO Auto-generated method stub
		arg0.writeDouble(median);
		arg0.writeDouble(standardDeviation);
	}

	@Override
	public String toString() {
		return median + "\\t" + standardDeviation;
	}

}
