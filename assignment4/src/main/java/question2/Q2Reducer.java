package question2;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class Q2Reducer extends Reducer<Text, CountAverageTuple, Text, CountAverageTuple> {

	final private CountAverageTuple result = new CountAverageTuple();

	@Override
	protected void reduce(Text keyIn, Iterable<CountAverageTuple> values, Context arg2)
			throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		double sum = 0, count = 0;

		for (CountAverageTuple v : values) {
			sum += v.getCount() * v.getAverage();
			count += v.getCount();
		}

		result.setAverage(sum / count);
		result.setCount(count);
		arg2.write(keyIn, result);
	}

}
