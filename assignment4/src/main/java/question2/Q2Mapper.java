package question2;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Q2Mapper extends Mapper<Object, Text, Text, CountAverageTuple> {

	final Text keyOut = new Text();
	final CountAverageTuple valOut = new CountAverageTuple();

	@Override
	protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		String row = value.toString();
		String[] columns = row.split(",");
		if (columns.length < 9)
			return;
		try {
			String[] date = StringUtils.split(columns[2], '-');
			if (date == null || date.length != 3) // eliminating the first row.
				return;
			keyOut.set(date[0]);
			valOut.setCount(1);
			valOut.setAverage(Double.parseDouble(columns[8]));
		} catch (NumberFormatException e) {
			System.out.println("corrupt input data");
			return;
		}
		context.write(keyOut, valOut);
	}

}
