package question4;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SortedMapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Q4Mapper extends Mapper<Object, Text, IntWritable, SortedMapWritable> {

	IntWritable keyOut = new IntWritable();

	@Override
	protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		String row = value.toString();
		String[] columns = row.split("::");
		if (columns.length < 4)// some data check
			return;

		SortedMapWritable valOut = new SortedMapWritable();
		try {
			keyOut.set(Integer.parseInt(columns[1]));
			DoubleWritable kVout = new DoubleWritable(Double.parseDouble(columns[2]));
			valOut.put(kVout, new LongWritable(1));
		} catch (NumberFormatException e) {
			// TODO: handle exception
			System.out.println(e);
		}
		context.write(keyOut, valOut);
	}
}
