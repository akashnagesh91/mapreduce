package question4;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SortedMapWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Reducer;

public class Q4Combiner extends Reducer<IntWritable, SortedMapWritable, IntWritable, SortedMapWritable> {

	@Override
	protected void reduce(IntWritable keyIn, Iterable<SortedMapWritable> values, Context context)
			throws IOException, InterruptedException {
		SortedMapWritable valOut = new SortedMapWritable();
		for (SortedMapWritable map : values) {
			Set<Entry<WritableComparable, Writable>> entrySet = map.entrySet();
			Entry<WritableComparable, Writable> next = entrySet.iterator().next();

			LongWritable presentValue = (LongWritable) valOut.get((DoubleWritable) next.getKey());

			if (presentValue == null) {
				valOut.put(next.getKey(), next.getValue());
			} else {
				valOut.put(next.getKey(),
						new LongWritable(presentValue.get() + ((LongWritable) (next.getValue())).get()));
			}

			map.clear();
		}
		context.write(keyIn, valOut);
	}

}
