package question4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SortedMapWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Reducer;

public class Q4Reducer extends Reducer<IntWritable, SortedMapWritable, IntWritable, MeanSDWritable> {

	final MeanSDWritable valOut = new MeanSDWritable();
	final List<Double> list = new LinkedList<>();

	@Override
	protected void reduce(IntWritable keyIn, Iterable<SortedMapWritable> values, Context context)
			throws IOException, InterruptedException {

		double sum = 0;
		long count = 0;
		list.clear();
		valOut.setMedian(0);
		valOut.setSd(0);

		for (SortedMapWritable sm : values) {
			for (Entry<WritableComparable, Writable> entry : sm.entrySet()) {
				long numberOfTimes = ((LongWritable) entry.getValue()).get();

				for (long i = 0; i < numberOfTimes; i++) {
					double val = ((DoubleWritable) entry.getKey()).get();
					list.add(val);
					sum += val;
				}
			}
			sm.clear();
		}
		count = list.size();
		Collections.sort(list);

		// Calculating median
		if (count % 2 == 0) {

			valOut.setMedian((list.get((int) count / 2 - 1) + list.get((int) count / 2)) / 2);
		} else {
			valOut.setMedian(list.get((int) count / 2));
		}

		// Calculating Standard deviation
		final double mean = sum / count;
		float sumOfSquares = 0;

		for (double i : list) {
			sumOfSquares += (i - mean) * (i - mean);
		}

		valOut.setSd(Math.sqrt(sumOfSquares / (count - 1)));

		context.write(keyIn, valOut);
	}

}
