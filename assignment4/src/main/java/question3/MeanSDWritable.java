package question3;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class MeanSDWritable implements Writable {

	double median = 0, sd = 0;

	@Override
	public void readFields(DataInput arg0) throws IOException {
		// TODO Auto-generated method stub
		median = arg0.readDouble();
		sd = arg0.readDouble();
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		// TODO Auto-generated method stub
		arg0.writeDouble(median);
		arg0.writeDouble(sd);

	}

	public double getMedian() {
		return median;
	}

	public void setMedian(double mean) {
		this.median = mean;
	}

	public double getSd() {
		return sd;
	}

	public void setSd(double sd) {
		this.sd = sd;
	}

	@Override
	public String toString() {
		return "median" + " :" + median + " " + "sd" + " :" + sd;
	}
}
