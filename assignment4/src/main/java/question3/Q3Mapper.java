package question3;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Q3Mapper extends Mapper<Object, Text, IntWritable, DoubleWritable> {

	final private IntWritable keyOut = new IntWritable();
	final private DoubleWritable valOut = new DoubleWritable();

	@Override
	protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
		String row = value.toString();
		String[] columns = row.split("::");
		if (columns.length < 4)// some data check
			return;
		try {
			keyOut.set(Integer.parseInt(columns[1]));
			valOut.set(Double.parseDouble(columns[2]));
		} catch (NumberFormatException e) {
			// TODO: handle exception
			System.out.println(e);
		}
		context.write(keyOut, valOut);
	}

}
