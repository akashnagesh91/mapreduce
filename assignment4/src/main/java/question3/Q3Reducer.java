package question3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class Q3Reducer extends Reducer<IntWritable, DoubleWritable, IntWritable, MeanSDWritable> {

	final private MeanSDWritable valOut = new MeanSDWritable();
	final private List<Double> list = new ArrayList<>();

	@Override
	protected void reduce(IntWritable keyIn, Iterable<DoubleWritable> values, Context context)
			throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		long sum = 0, count = 0;
		list.clear();

		for (DoubleWritable iw : values) {
			list.add(iw.get());
			sum += iw.get();
			++count;
		}

		Collections.sort(list);

		// Calculating median
		if (count % 2 == 0) {

			valOut.setMedian((list.get((int) count / 2 - 1) + list.get((int) count / 2)) / 2);
		} else {
			valOut.setMedian(list.get((int) count / 2));
		}

		// Calculating Standard deviation
		final float mean = sum / count;
		float sumOfSquares = 0;

		for (double i : list) {
			sumOfSquares += (i - mean) * (i - mean);
		}

		valOut.setSd(Math.sqrt(sumOfSquares / (count - 1)));

		context.write(keyIn, valOut);
	}

	@Override
	protected void cleanup(Reducer<IntWritable, DoubleWritable, IntWritable, MeanSDWritable>.Context context)
			throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		list.clear();
	}

}
