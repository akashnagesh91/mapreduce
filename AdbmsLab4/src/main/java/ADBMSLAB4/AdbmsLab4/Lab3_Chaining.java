package ADBMSLAB4.AdbmsLab4;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Lab3_Chaining {
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		cleanUpOutputDiectory(conf, args[1], args[2]);
		Job job1 = Job.getInstance(conf, "chaining");

		job1.setJarByClass(Lab3_Chaining.class);
		job1.setMapperClass(Map1.class);
		job1.setMapOutputKeyClass(Text.class);
		job1.setMapOutputValueClass(IntWritable.class);

		job1.setReducerClass(Reduce1.class);
		job1.setOutputKeyClass(Text.class);
		job1.setOutputValueClass(IntWritable.class);

		FileInputFormat.addInputPath(job1, new Path(args[0]));
		FileOutputFormat.setOutputPath(job1, new Path(args[1]));
		job1.waitForCompletion(true);

		Job job2 = Job.getInstance(conf, "chaining");

		job2.setJarByClass(Lab3_Chaining.class);
		job2.setMapperClass(Map2.class);
		job2.setMapOutputKeyClass(IntWritable.class);
		job2.setMapOutputValueClass(Text.class);

		job2.setReducerClass(Reduce2.class);
		job2.setOutputKeyClass(Text.class);
		job2.setOutputValueClass(IntWritable.class);

		FileInputFormat.addInputPath(job2, new Path(args[1]));
		FileOutputFormat.setOutputPath(job2, new Path(args[2]));
		System.exit(job2.waitForCompletion(true) ? 0 : 1);
	}

	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {
			if (p != null) {
				Path outPath = new Path(p);
				outPath.getFileSystem(new Configuration()).delete(outPath, true);
			}
		}
	}

	public static class Map1 extends Mapper<LongWritable, Text, Text, IntWritable> {

		// private final static IntWritable sales = new IntWritable();
		// private Text employee = new Text();

		@Override
		public void map(LongWritable key, Text value, Context context) {
			String row[] = value.toString().split(",");
			String employeeId = row[2];

			String salesAmount = row[3].trim();

			try {
				IntWritable sales = new IntWritable(Integer.parseInt(salesAmount));
				context.write(new Text(employeeId), sales);
			} catch (Exception e) {
				System.out.println(e);
			}
		}

	}

	public static class Reduce1 extends Reducer<Text, IntWritable, Text, IntWritable> {

		private IntWritable totalSales = new IntWritable();

		@Override
		public void reduce(Text key, Iterable<IntWritable> values,
				Reducer<Text, IntWritable, Text, IntWritable>.Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			totalSales.set(sum);
			context.write(key, totalSales);
		}

	}

	public static class Map2 extends Mapper<LongWritable, Text, IntWritable, Text> {

		@Override
		public void map(LongWritable key, Text value, Mapper<LongWritable, Text, IntWritable, Text>.Context context)
				throws IOException, InterruptedException {
			String row[] = value.toString().split("\\t");
			Text employeeId = new Text(row[0]);
			String salesAmount = row[1].trim();

			try {
				IntWritable count = new IntWritable(Integer.parseInt(salesAmount));
				context.write(count, employeeId);
			} catch (Exception e) {
			}
		}
	}

	public static class Reduce2 extends Reducer<IntWritable, Text, Text, IntWritable> {

		@Override
		public void reduce(IntWritable key, Iterable<Text> value, Context context)
				throws IOException, InterruptedException {
			System.out.println("inside reducer=============");
			for (Text val : value) {
				context.write(val, key);
			}
		}
	}
}
