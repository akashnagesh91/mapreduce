package AirbnbAnalysis.analysis1;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ListingsPerHostReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

	@Override
	protected void reduce(Text keyIn, Iterable<IntWritable> values, Context context)
			throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		int numberOfHosts = 0;
		for (IntWritable i : values) {
			numberOfHosts += i.get();
		}
		if (keyIn.toString().length() > 2)
			context.write(keyIn, new IntWritable(numberOfHosts));
	}

}
