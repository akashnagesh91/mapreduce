package AirbnbAnalysis.analysis1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class ListingsPerHostMapper extends Mapper<Object, Text, Text, IntWritable> {

	private final static IntWritable ONE = new IntWritable(1);
	private final static String[] keyList = { "", "ONE", "TWO", "THREE", "FOUR", "FIVE+" };

	@Override
	protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		final String[] inputArray = value.toString().split("\t");
		final int numberOfColumns = inputArray.length;
		if (numberOfColumns < 90 || numberOfColumns > 95) {
			return;
		}

		IntWritable keyOut = null;

		try {
			int hostListingsCount = Integer.parseInt(inputArray[32]);
			if (hostListingsCount > 5)
				hostListingsCount = 5;
			context.write(new Text(keyList[hostListingsCount]), ONE);
		} catch (Exception e) {
			// Ignore and go to next row
			return;
		}
	}

}
// shopping and historic sites