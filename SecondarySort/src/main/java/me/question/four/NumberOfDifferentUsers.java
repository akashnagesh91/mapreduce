package me.question.four;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class NumberOfDifferentUsers {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out.println("printing the mapper value");
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, "Question 4");
			job.setJarByClass(NumberOfDifferentUsers.class);
			job.setMapperClass(UserCountMapper.class);
			job.setReducerClass(UserCountReducer.class);
			job.setCombinerClass(UserCountReducer.class);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(IntWritable.class);

			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileOutputFormat.setOutputPath(job, new Path(args[1]));

			cleanUpOutputDiectory(conf, args[1]);

			System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (InterruptedException e) {
			System.out.println(e);
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		}
	}

	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {
			if (p != null) {
				Path outPath = new Path(p);
				outPath.getFileSystem(new Configuration()).delete(outPath, true);
			}
		}
	}

}
