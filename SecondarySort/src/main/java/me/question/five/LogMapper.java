package me.question.five;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class LogMapper extends Mapper<Object, Text, Text, IntWritable> {

	private final static IntWritable valueOut = new IntWritable(1);
	private final Text keyOut = new Text();

	@Override
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		String row = value.toString();
		String[] columns = row.split(" - -");
		if (columns.length < 2) {
			return;
		}

		keyOut.set(columns[0]);
		context.write(keyOut, valueOut);

	}
}
