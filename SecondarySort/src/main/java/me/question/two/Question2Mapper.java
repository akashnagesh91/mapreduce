package me.question.two;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Question2Mapper extends Mapper<Object, Text, Text, DoubleWritable> {

	final DoubleWritable valueOut = new DoubleWritable();
	final Text keyOut = new Text();

	@Override
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
		String row = value.toString();
		String[] columns = row.split(",");
		if (columns.length < 9)
			return;
		try {
			this.valueOut.set(Double.parseDouble(columns[4]));
		} catch (NumberFormatException e) {
			// This is the first line in the file.
			return;
		}
		keyOut.set(columns[1]);
		context.write(keyOut, valueOut);
	}

}
