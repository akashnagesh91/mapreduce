package me.question.two;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class FindAverage {

	public static void main(String args[]) throws Exception {

		try {
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, "Average calculator");
			job.setJarByClass(FindAverage.class);
			job.setMapperClass(Question2Mapper.class);
			job.setReducerClass(Question2Reducer.class);
			// job.setCombinerClass(ReducerWordCount.class);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(DoubleWritable.class);

			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileOutputFormat.setOutputPath(job, new Path(args[1]));

			cleanUpOutputDiectory(conf, args[1]);
			
			System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (InterruptedException e) {
			System.out.println(e);
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		}
	}
	
	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {
			if (p != null) {
				Path outPath = new Path(p);
				outPath.getFileSystem(new Configuration()).delete(outPath, true);
			}
		}
	}

}
