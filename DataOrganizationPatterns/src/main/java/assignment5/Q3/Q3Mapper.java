package assignment5.Q3;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Q3Mapper extends Mapper<Object, Text, Text, Text> {

	Text keyOut = new Text();

	@Override
	protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
		String row = value.toString();
		String[] columns = row.split("/");
		if (columns.length < 3) {
			return;
		}
		keyOut.set(columns[1]);
		context.write(keyOut, value);
	}

}
