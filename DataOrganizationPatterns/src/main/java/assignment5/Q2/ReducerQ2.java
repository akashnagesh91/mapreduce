package assignment5.Q2;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ReducerQ2 extends Reducer<Text, NullWritable, Text, NullWritable> {

	@Override
	protected void reduce(Text arg0, Iterable<NullWritable> arg1, Context context)
			throws IOException, InterruptedException {
		context.write(arg0, NullWritable.get());
	}

}
