package assignment5.Q2;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Q2Mapper extends Mapper<Object, Text, Text, NullWritable> {

	private Text keyOut = new Text();

	@Override
	protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
		String row = value.toString();
		String[] columns = row.split(" - -");
		if (columns.length < 2) {
			return;
		}

		keyOut.set(columns[0]);
		context.write(keyOut, NullWritable.get());

	}

}
