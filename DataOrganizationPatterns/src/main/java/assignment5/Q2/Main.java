package assignment5.Q2;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Main {

	public static void main(String[] args) throws IOException {
		try {
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, "Question 2");
			job.setJarByClass(Main.class);
			job.setMapperClass(Q2Mapper.class);
			job.setReducerClass(ReducerQ2.class);
			job.setCombinerClass(ReducerQ2.class);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(NullWritable.class);

			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileOutputFormat.setOutputPath(job, new Path(args[1]));
			cleanUpOutputDiectory(conf, args[1]);
			System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (InterruptedException e) {
			System.out.println(e);
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		}

	}

	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {

			Path outPath = new Path(p);
			outPath.getFileSystem(conf).delete(outPath, true);

		}
	}

}
