package assignment5.Q4;

import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

public class Q4Mapper extends Mapper<Object, Text, Text, NullWritable> {

	MultipleOutputs<Text, NullWritable> mout = null;
	//Text keyOut = new Text();

	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		this.mout = new MultipleOutputs<>(context);
	}

	@Override
	protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
		String row = value.toString();
		String[] columns = row.split(":");
		if (columns.length < 3) {
			return;
		}
		String hr = columns[1];
		//keyOut.set(columns[1]);
		mout.write(value, NullWritable.get(), hr + "_hours");
	}

	@Override
	protected void cleanup(Mapper<Object, Text, Text, NullWritable>.Context context)
			throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		this.mout.close();
	}
}
