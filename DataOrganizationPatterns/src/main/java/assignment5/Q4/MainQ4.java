package assignment5.Q4;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class MainQ4 {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		try {
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, "Question 4");
			job.setJarByClass(MainQ4.class);
			job.setMapperClass(Q4Mapper.class);
			// job.setReducerClass(Q4Reducer.class);
			// job.setCombinerClass(Q3Reducer.class);
			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(NullWritable.class);
			// job.setOutputKeyClass(Text.class);
			// job.setOutputValueClass(NullWritable.class);

			job.setNumReduceTasks(0);

			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileOutputFormat.setOutputPath(job, new Path(args[1]));
			cleanUpOutputDiectory(conf, args[1]);

			///////////////// MULTIPLE OUTPUTS

			MultipleOutputs.addNamedOutput(job, "bin", TextOutputFormat.class, Text.class, NullWritable.class);
			MultipleOutputs.setCountersEnabled(job, true);
			System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (InterruptedException e) {
			System.out.println(e);
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		}

	}

	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {

			Path outPath = new Path(p);
			outPath.getFileSystem(conf).delete(outPath, true);

		}
	}

}
