package xml;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class StructuralToHierarchial {

	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		// TODO Auto-generated method stub

		Configuration conf = new Configuration();
		Job job = new Job(conf, "PostCommentHierarchy");
		job.setJarByClass(StructuralToHierarchial.class);

		MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, MovieMapper.class);

		MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, TagsMapper.class);

		job.setReducerClass(MovieTagReducer.class);

		job.setOutputFormatClass(TextOutputFormat.class);
		TextOutputFormat.setOutputPath(job, new Path(args[2]));

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		cleanUpOutputDiectory(conf, args[2]);

		System.exit(job.waitForCompletion(true) ? 0 : 2);

	}

	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {

			Path outPath = new Path(p);
			outPath.getFileSystem(conf).delete(outPath, true);

		}
	}

	private static class MovieMapper extends Mapper<Object, Text, Text, Text> {

		final private Text outKey = new Text();

		@Override
		protected void map(Object key, Text value, Mapper<Object, Text, Text, Text>.Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			final String val[] = value.toString().split(",");
			outKey.set(val[0]);
			StringBuilder sb = new StringBuilder();
			sb.append("M").append(val[1]);
			context.write(outKey, new Text(sb.toString()));

		}
	}

	private static class TagsMapper extends Mapper<Object, Text, Text, Text> {
		final private Text outKey = new Text();
		final private Text outValue = new Text();

		@Override
		protected void map(Object key, Text value, Mapper<Object, Text, Text, Text>.Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			final String val[] = value.toString().split(",");
			outKey.set(val[1]);
			StringBuilder sb = new StringBuilder();
			sb.append("T").append(val[2]);
			// System.out.println(sb.toString());
			outValue.set(sb.toString());
			context.write(outKey, outValue);
		}
	}

	private static class MovieTagReducer extends Reducer<Text, Text, Text, NullWritable> {

		private final List<String> tags = new ArrayList<String>();

		@Override
		protected void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, NullWritable>.Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			tags.clear();
			for (Text t : values) {
				if (t != null) {
					if (t.toString().charAt(0) == 'T') {
						tags.add(t.toString().substring(1, t.toString().length()).trim());
					} else {
						key = new Text(t.toString().substring(1, t.toString().length()).trim());

					}
				}
			}

			if (key != null) {
				String movieWithTagChildren = "";
				try {
					movieWithTagChildren = transformToXml(key.toString(), tags);
				} catch (TransformerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				context.write(new Text(movieWithTagChildren), NullWritable.get());
			}
		}

		private String transformToXml(String movieId, List<String> tags)
				throws TransformerException, ParserConfigurationException {

			DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
			DocumentBuilder build = dFact.newDocumentBuilder();
			Document doc = build.newDocument();
			Element root = doc.createElement("root");
			doc.appendChild(root);
			Element movieWithItsTags = doc.createElement("movieWithItsTags");
			root.appendChild(movieWithItsTags);
			Element movie = doc.createElement("Movie");
			movie.setAttribute("MovieTitle", movieId);
			movieWithItsTags.appendChild(movie);
			for (String tag : tags) {
				Element movieTag = doc.createElement("Tag");
				movieTag.appendChild(doc.createTextNode(tag.trim()));
				movie.appendChild(movieTag);
			}
			TransformerFactory tFact = TransformerFactory.newInstance();
			Transformer trans = tFact.newTransformer();

			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);

			return writer.toString();

		}

	}

}
