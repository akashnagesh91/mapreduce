package question4;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Q4Main {
	public static void main(String args[]) throws InterruptedException {
		try {
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, "Question4");
			job.setJarByClass(Q4Main.class);
			job.setMapperClass(Q4Mapper.class);

			job.setReducerClass(Q4Reducer.class);
			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(CustomWritableForMapperOutput.class);
			// job.setGroupingComparatorClass(GroupingComparator.class);

			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(CustomWritableForReducerOutput.class);
			// job.setInputFormatClass(TextInputFormat.class);

			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileOutputFormat.setOutputPath(job, new Path(args[1]));

			cleanUpOutputDiectory(conf, args[1]);

			System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (IOException | ClassNotFoundException e) {
			Logger.getLogger(Q4Main.class.getName()).log(Level.SEVERE, "MR fucked", e);
		}
	}

	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {
			if (p != null) {
				Path outPath = new Path(p);
				outPath.getFileSystem(new Configuration()).delete(outPath, true);
			}
		}
	}
}
