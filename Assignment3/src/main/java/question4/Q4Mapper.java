package question4;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Q4Mapper extends Mapper<Object, Text, Text, CustomWritableForMapperOutput> {

	final Text keyOut = new Text();
	CustomWritableForMapperOutput customWritable = new CustomWritableForMapperOutput();

	@Override
	protected void map(Object key, Text value,
			Mapper<Object, Text, Text, CustomWritableForMapperOutput>.Context context)
			throws IOException, InterruptedException {
		String row = value.toString();
		String[] columns = row.split(",");
		if (columns.length < 9)
			return;
		try {
			String date = columns[2];
			double stockVolume = Double.parseDouble(columns[7]);
			double stockPriceAdjClose =Double.parseDouble(columns[8]);
			customWritable.setDate(date);
			customWritable.setStockPriceAdjClose(stockPriceAdjClose);
			customWritable.setStockVolume(stockVolume);
		} catch (NumberFormatException e) {
			// This is the first line in the file.
			return;
		}
		keyOut.set(columns[1]);
		context.write(keyOut, customWritable);
	}

}
