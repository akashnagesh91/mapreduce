package question4;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class Q4Reducer extends Reducer<Text, CustomWritableForMapperOutput, Text, CustomWritableForReducerOutput> {

	@Override
	protected void reduce(Text keyIn, Iterable<CustomWritableForMapperOutput> valueIn, Context context)
			throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		String dateForMaxStockVolume = StringUtils.EMPTY, dateForMinStockVolume = StringUtils.EMPTY;
		double maxStockPriceAdjClose = (Double.MIN_VALUE), maxStockVolume = (Double.MIN_VALUE);
		double minStockVolume = Double.MAX_VALUE;

		for (CustomWritableForMapperOutput mapperOutput : valueIn) {
			if (mapperOutput.getStockVolume() > (maxStockVolume)) {
				maxStockVolume = mapperOutput.getStockVolume();
				dateForMaxStockVolume = mapperOutput.getDate();
			}

			if (mapperOutput.getStockVolume() < (minStockVolume)) {
				minStockVolume = mapperOutput.getStockVolume();
				dateForMinStockVolume = mapperOutput.getDate();
			}

			if (mapperOutput.getStockPriceAdjClose() > (maxStockPriceAdjClose)) {
				maxStockPriceAdjClose = mapperOutput.getStockPriceAdjClose();
			}
		}
		CustomWritableForReducerOutput customWritableForReducerOutput = new CustomWritableForReducerOutput(
				dateForMaxStockVolume, dateForMinStockVolume, maxStockPriceAdjClose);
		context.write(keyIn, customWritableForReducerOutput);
	}
}
