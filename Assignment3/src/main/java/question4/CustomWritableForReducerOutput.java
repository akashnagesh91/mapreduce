package question4;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;

public class CustomWritableForReducerOutput implements Writable {

	private String dateMaxStockVolume, dateMinStockVolume;
	private double stockPriceAdjClose;

	public CustomWritableForReducerOutput() {
		// TODO Auto-generated constructor stub
	}

	public CustomWritableForReducerOutput(String dateMaxStockVolume, String dateMinStockVolume,
			double stockPriceAdjClose) {
		this.dateMaxStockVolume = dateMaxStockVolume;
		this.dateMinStockVolume = dateMinStockVolume;
		this.stockPriceAdjClose = stockPriceAdjClose;
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
		// TODO Auto-generated method stub
		dateMaxStockVolume = WritableUtils.readString(arg0);
		dateMinStockVolume = WritableUtils.readString(arg0);
		stockPriceAdjClose = arg0.readDouble();
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		// TODO Auto-generated method stub
		WritableUtils.writeString(arg0, dateMaxStockVolume);
		WritableUtils.writeString(arg0, dateMinStockVolume);
		arg0.writeDouble(stockPriceAdjClose);
	}

	public String getDateMaxStockVolume() {
		return dateMaxStockVolume;
	}

	public void setDateMaxStockVolume(String dateMaxStockVolume) {
		this.dateMaxStockVolume = dateMaxStockVolume;
	}

	public String getDateMinStockVolume() {
		return dateMinStockVolume;
	}

	public void setDateMinStockVolume(String dateMinStockVolume) {
		this.dateMinStockVolume = dateMinStockVolume;
	}

	public double getStockPriceAdjClose() {
		return stockPriceAdjClose;
	}

	public void setStockPriceAdjClose(double stockPriceAdjClose) {
		this.stockPriceAdjClose = stockPriceAdjClose;
	}

	@Override
	public String toString() {
		return new StringBuilder().append(dateMaxStockVolume).append("\t").append(dateMinStockVolume).append("\t")
				.append(stockPriceAdjClose).toString();
	}
}
