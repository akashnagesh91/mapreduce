package question4;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;

public class CustomWritableForMapperOutput implements Writable {
	private String date;
	private double stockVolume, stockPriceAdjClose;

	public CustomWritableForMapperOutput() {
		// TODO Auto-generated constructor stub
	}

	public CustomWritableForMapperOutput(String stockSymbol, String date, double stockVolume,
			double stockPriceAdjClose) {
		this.date = date;
		this.stockVolume = stockVolume;
		this.stockPriceAdjClose = stockPriceAdjClose;
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
		// TODO Auto-generated method stub
		this.date = WritableUtils.readString(arg0);
		this.stockVolume = arg0.readDouble();
		this.stockPriceAdjClose = arg0.readDouble();
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		// TODO Auto-generated method stub
		WritableUtils.writeString(arg0, date);
		arg0.writeDouble(stockVolume);
		arg0.writeDouble(stockPriceAdjClose);
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getStockVolume() {
		return stockVolume;
	}

	public void setStockVolume(double stockVolume) {
		this.stockVolume = stockVolume;
	}

	public double getStockPriceAdjClose() {
		return stockPriceAdjClose;
	}

	public void setStockPriceAdjClose(double stockPriceAdjClose) {
		this.stockPriceAdjClose = stockPriceAdjClose;
	}

	@Override
	public String toString() {
		return new StringBuilder().append(date).append("\t").append(stockVolume).append("\t").append(stockPriceAdjClose)
				.toString();
	}
}
