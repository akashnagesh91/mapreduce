package question5;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Q5Mapper2 extends Mapper<Object, Text, DoubleWritable, IntWritable> {

	private final DoubleWritable keyOut = new DoubleWritable();
	private final IntWritable valOut = new IntWritable();

	@Override
	protected void map(Object key, Text value, Mapper<Object, Text, DoubleWritable, IntWritable>.Context context)
			throws IOException, InterruptedException {
		final String[] column = value.toString().split("\\t");
		try {
			this.keyOut.set(Double.parseDouble(column[1]));
			this.valOut.set(Integer.parseInt(column[0]));
		} catch (NumberFormatException e) {
			System.out.println(e);
		}
		context.write(keyOut, valOut);
	}

}
