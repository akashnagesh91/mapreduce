package question5;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class Q5Mapper1 extends Mapper<Object, Text, IntWritable, DoubleWritable> {

	private final IntWritable keyOut = new IntWritable();
	private final DoubleWritable valOut = new DoubleWritable();

	@Override
	protected void map(Object key, Text value, Mapper<Object, Text, IntWritable, DoubleWritable>.Context context)
			throws IOException, InterruptedException {
		String row = value.toString();
		String[] columns = row.split("::");
		if (columns.length < 4)
			return;
		try {
			keyOut.set(Integer.parseInt(columns[1]));
			valOut.set(Double.parseDouble(columns[2]));
		} catch (NumberFormatException e) {
			System.out.println(e);
			return;
		}
		context.write(keyOut, valOut);
	}
}
