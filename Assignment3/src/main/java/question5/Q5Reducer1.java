package question5;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class Q5Reducer1 extends Reducer<IntWritable, DoubleWritable, IntWritable, DoubleWritable> {

	private final DoubleWritable valOut = new DoubleWritable();

	@Override
	protected void reduce(IntWritable keyIn, Iterable<DoubleWritable> valIn, Context context)
			throws IOException, InterruptedException {
		double totalRating = 0;
		int count = 0;
		for (DoubleWritable rating : valIn) {
			totalRating += rating.get();
			count++;
		}

		valOut.set(totalRating / count);
		context.write(keyIn, valOut);
	}

}
