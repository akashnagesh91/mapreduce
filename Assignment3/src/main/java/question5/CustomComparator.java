package question5;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * 
 * @author akashnagesh This is a custom comparator that the hadoop framework
 *         should ideally be using to sort the keys before they are consumed by
 *         the reducer. I implement my own compare method here.
 */
public class CustomComparator extends WritableComparator {

	public CustomComparator() {
		super(DoubleWritable.class, true);
	}

	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		// TODO Auto-generated method stub
		return -1 * a.compareTo(b);
	}

}
