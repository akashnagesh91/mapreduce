package question5;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Q5Main {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		try {
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, "Question5");
			job.setJarByClass(Q5Main.class);
			job.setMapperClass(Q5Mapper1.class);

			job.setReducerClass(Q5Reducer1.class);
			job.setMapOutputKeyClass(IntWritable.class);
			job.setMapOutputValueClass(DoubleWritable.class);
			// job.setGroupingComparatorClass(GroupingComparator.class);

			job.setOutputKeyClass(IntWritable.class);
			job.setOutputValueClass(DoubleWritable.class);
			// job.setInputFormatClass(TextInputFormat.class);

			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileOutputFormat.setOutputPath(job, new Path(args[1]));

			cleanUpOutputDiectory(conf, args[1], args[2]);

			job.waitForCompletion(true);

			Job job1 = Job.getInstance(conf, "chaining");
			job1.setJarByClass(Q5Main.class);
			job1.setMapperClass(Q5Mapper2.class);

			job1.setReducerClass(Q5Reducer2.class);
			job1.setMapOutputKeyClass(DoubleWritable.class);
			job1.setMapOutputValueClass(IntWritable.class);

			job1.setSortComparatorClass(CustomComparator.class);

			job1.setOutputKeyClass(IntWritable.class);
			job1.setOutputValueClass(DoubleWritable.class);

			job1.setNumReduceTasks(1);
			

			FileInputFormat.setInputPaths(job1, new Path(args[1]));
			FileOutputFormat.setOutputPath(job1, new Path(args[2]));

			System.exit(job1.waitForCompletion(true) ? 0 : 1);

		} catch (IOException | ClassNotFoundException e) {
			Logger.getLogger(Q5Main.class.getName()).log(Level.SEVERE, "MR fucked", e);
		}

	}

	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {
			if (p != null) {
				Path outPath = new Path(p);
				outPath.getFileSystem(new Configuration()).delete(outPath, true);
			}
		}
	}
}
