package question5;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class Q5Reducer2 extends Reducer<DoubleWritable, IntWritable, IntWritable, DoubleWritable> {

	private static int count = 0;

	@Override
	protected void reduce(DoubleWritable keyIn, Iterable<IntWritable> valIn, Context context)
			throws IOException, InterruptedException {
		++count;
		if (count > 25)
			return;
		context.write(valIn.iterator().next(), keyIn);
	}

}
