package question3;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Q3KeyValueTextInputFormat {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		try {
			Configuration conf = new Configuration();
			Job job = Job.getInstance(conf, "Question3");
			job.setJarByClass(Q3KeyValueTextInputFormat.class);
			job.setMapperClass(MapperForKeyValueTextInputFormat.class);

			job.setReducerClass(ReducerForKeyValueTextInputFormat.class);
			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(IntWritable.class);
			// job.setGroupingComparatorClass(GroupingComparator.class);

			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(IntWritable.class);
			// job.setInputFormatClass(TextInputFormat.class);

			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileOutputFormat.setOutputPath(job, new Path(args[1]));

			job.setInputFormatClass(KeyValueTextInputFormat.class);

			cleanUpOutputDiectory(conf, args[1]);

			System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (IOException | ClassNotFoundException e) {
			Logger.getLogger(Q3KeyValueTextInputFormat.class.getName()).log(Level.SEVERE, "MR fucked", e);
		}
	}

	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {
			if (p != null) {
				Path outPath = new Path(p);
				outPath.getFileSystem(new Configuration()).delete(outPath, true);
			}
		}
	}

	public static class MapperForKeyValueTextInputFormat extends Mapper<Text, Text, Text, IntWritable> {

		private final static IntWritable one = new IntWritable(1);
		private final Text word = new Text();

		@Override
		public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
			context.write(key, new IntWritable(Integer.parseInt(value.toString())));
		}

	}

	public static class ReducerForKeyValueTextInputFormat extends Reducer<Text, IntWritable, Text, IntWritable> {

		@Override
		protected void reduce(Text keyIn, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			int sum = 0;

			for (IntWritable val : values) {
				sum += val.get();
			}

			context.write(keyIn, new IntWritable(sum));
		}

	}

}
