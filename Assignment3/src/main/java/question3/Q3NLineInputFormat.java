package question3;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.NLineInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/***
 * 
 * @author akashnagesh This is a simple mapreduce program to demonstrate
 *         NLineInputFormat.
 * 
 *         The data set in consideration is a unique record every 3 lines.
 * 
 */
public class Q3NLineInputFormat {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		try {
			Configuration conf = new Configuration();
			conf.setInt(NLineInputFormat.LINES_PER_MAP, 3);
			Job job = Job.getInstance(conf, "Question3");
			job.setJarByClass(Q3NLineInputFormat.class);
			job.setMapperClass(Mapper1.class);

			job.setReducerClass(Reducer1.class);
			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(Text.class);
			// job.setGroupingComparatorClass(GroupingComparator.class);

			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(Text.class);
			// job.setInputFormatClass(TextInputFormat.class);

			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileOutputFormat.setOutputPath(job, new Path(args[1]));

			job.setInputFormatClass(NLineInputFormat.class);

			cleanUpOutputDiectory(conf, args[1]);

			System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (IOException | ClassNotFoundException e) {
			Logger.getLogger(Q3NLineInputFormat.class.getName()).log(Level.SEVERE, "MR fucked", e);
		}
	}

	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {
			if (p != null) {
				Path outPath = new Path(p);
				outPath.getFileSystem(new Configuration()).delete(outPath, true);
			}
		}
	}

	public static class Mapper1 extends Mapper<Object, Text, Text, Text> {

		@Override
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			context.write(value, value);
		}

	}

	public static class Reducer1 extends Reducer<Text, Text, Text, Text> {

		@Override
		protected void reduce(Text keyIn, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			context.write(keyIn, values.iterator().next());
		}

	}

}
