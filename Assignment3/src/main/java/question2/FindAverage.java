package question2;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class FindAverage {

	public static void main(String args[]) throws Exception {
		Configuration conf = new Configuration();

		FindAverage.cleanUpOutputDiectory(conf, args[1], args[3]);

		final long startTimeWithoutMerge = System.currentTimeMillis();
		FindAverage.runJob(args[0], args[1], conf);
		final long endTimeWithoutMerge = System.currentTimeMillis();

		System.out.println("Time taken to complete the job without merge is: "
				+ (endTimeWithoutMerge - startTimeWithoutMerge) + "ms");
		Logger logger = Logger.getLogger("merge_logger");

		logger.log(Level.WARNING, "Time taken to complete the job without merge is: "
				+ (endTimeWithoutMerge - startTimeWithoutMerge) + "ms");

		final long startTimeWithoutMerge1 = System.currentTimeMillis();
		FindAverage.mergeFiles(conf, args[0], args[2]);
		FindAverage.runJob(args[2], args[3], conf);
		final long endTimeWithoutMerge1 = System.currentTimeMillis();

		System.out.println("Time taken to complete the job with merge is: "
				+ (endTimeWithoutMerge1 - startTimeWithoutMerge1) + "ms");
		logger.log(Level.WARNING, "Time taken to complete the job with merger is: "
				+ (endTimeWithoutMerge1 - startTimeWithoutMerge1) + "ms");

	}

	public static void cleanUpOutputDiectory(Configuration conf, String... path) throws IOException {

		for (String p : path) {
			if (p != null) {
				Path outPath = new Path(p);
				outPath.getFileSystem(conf).delete(outPath, true);
			}
		}
	}

	public static void runJob(String arg0, String arg1, Configuration conf) throws Exception {
		try {
			Job job = Job.getInstance(conf, "Average calculator");
			job.setJarByClass(FindAverage.class);
			job.setMapperClass(Question2Mapper.class);
			job.setReducerClass(Question2Reducer.class);
			// job.setCombinerClass(ReducerWordCount.class);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(DoubleWritable.class);

			FileInputFormat.addInputPath(job, new Path(arg0));
			FileOutputFormat.setOutputPath(job, new Path(arg1));

			// System.out.println("job status ==========="+job.isComplete());

			job.waitForCompletion(true);
			// System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (InterruptedException e) {
			System.out.println(e);
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		}
	}

	public static void mergeFiles(Configuration conf, String args0, String args1) throws Exception {
		Path inputDir = new Path(args0);
		Path hdfsFile = new Path(args1);

		FileSystem hdfs = hdfsFile.getFileSystem(conf);
		FileSystem local = inputDir.getFileSystem(conf);

		try {
			FileStatus[] inputFiles = local.listStatus(inputDir);
			FSDataOutputStream out = hdfs.create(hdfsFile);

			for (int i = 0; i < inputFiles.length; i++) {
				FSDataInputStream in = local.open(inputFiles[i].getPath());

				byte buffer[] = new byte[256];
				int bytesRead = 0;
				while ((bytesRead = in.read(buffer)) > 0) {
					out.write(buffer, 0, bytesRead);
				}
				in.close();
			}
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
